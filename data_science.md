# Data Science

- [Mathematics](mathematics.md)
- [Python Tools](python_tools.md)
- [Gathering Data](gathering_data.md)
- [Machine Learning](machine_learn.md)
- [Deep Learning](deep_learning.md)
- [Natural Language Processing](natural_language_processing.md)
- [Gelling With Data Science Community](gelling_with_data_science_community.md)
