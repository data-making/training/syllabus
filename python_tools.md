
# Python tools

* Jupyter Lab
* Data Visualization
  * Visuals that helped understand things
    * Napoleon invasion of Russia
    * London cholera outbreak
  * Graphing with Seaborne and Matplotlib
* Dataframes with Pandas
  * Fetching data
  * Reading data
  * Filtering data
  * Computing with data
  * Dealing with missing values
  * Graphing with Dataframes
