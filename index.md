# Training Syllabus

- [GNU/Linux](gnu_linux.md)
- [Programming](programming.md)
- [Data Science](data_science.md)
- [Data Engineering](data_engineering.md)
