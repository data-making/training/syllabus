# Python

## Introduction


## Variables


## Operators


## Data Types

* Text Type
 * str

* Numeric Types
 * int
 * float
 * complex

* Sequence Types
 * list
 * tuple
 * range

* Mapping Type
 * dict

* Set Types
 * set
 * frozenset

* Boolean Type
 * bool

* Binary Types
 * bytes
 * bytearray
 * memoryview

* None Type
 * None

## Functions


## User Input


## Control Flow/Flow Control

* if .. else Statements

* Loops
 * for
 * while

* Control Statements
 * pass
 * break
 * continue

## Modules and Packages


## Date and DateTime Operations

* date
* time
* datetime

## File Handling


## OOP(Object Oriented Programming)

* Class
* Object
* Inheritance


## Exception Handling


## Logging


## Lambda