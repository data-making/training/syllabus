# Data Engineering

## Good/must to have Skills [It will be covered as common skill set or part of DS/DE]

* Linux Commands
* Shell Scripting
* Database Concepts
* SQL

## Big Data

* Introduction to Big Data
* Characteristics of Big Data
* Common Big Data Use Cases

## Hadoop

* Hadoop
  * HDFS
  * MapReduce

* YARN
  * YARN Concepts

* Hive
  * Hive Concepts

* Sqoop
  * Sqoop Import
  * Sqoop Export

* HBase
  * HBase Concepts

* Oozie
  * Oozie Concepts

## Apache Airflow

* Apache Airflow Concepts

## Apache Spark(PySpark[Apache Spark + Python])

* Spark Code API
* Spark SQL
  * DataFrame based operations
  * SQL based operations
* Spark Streaming
  * DStream Based
  * Structured Streaming(DataFrame based)
* Spark ML

## Apache Kafka

* Pub-Sub Concepts
* Kafka Connect
* Kafka Streams
* KSQL

## Cloud Computing

* AWS Cloud
  * Data Engineering based services

* Azure Cloud
  * Data Engineering based services

* GCP Cloud
  * Data Engineering based services

## Study Projects using Data Engineering

* Building Real-time Data Pipeline using On-premise based Data Engineering technologies

* Building Batch Data Pipeline using On-premise based Data Engineering technologies

* Building Real-time Data Pipeline using Cloud based Data Engineering services/technologies

* Building Batch Data Pipeline using Cloud based Data Engineering services/technologies

