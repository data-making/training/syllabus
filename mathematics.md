# Mathematics

* Statistics
  * Total
  * Average
  * Mean
  * Median
  * Mode
  * Outliers
  * Variance
  * Bell Curve
- Geometry
  - Point
  - Line
  - Plane
  - Equations
  - Polynomials
- Probability
- Matrices
- Calculus
